// Load the testing framework.
const test = require('ava');

// Load the electrum library.
const  { ElectrumClient, ElectrumTransport } = require('electrum-cash');

// Load the server list.
const serverList = require('../serverlist.js');

// Set our default minimum version.
const minVersion = '1.4.1';

/**
 * Tests if a server responds on a given transport scheme.
 *
 * @param {string} host              fully qualified domain name or IP number of the host.
 * @param {number} port              the TCP network port of the host.
 * @param {TransportScheme} scheme   the transport scheme to use for connection
 */
const testServerTransport = async function(host, port, scheme, t)
{
	try
	{
		// Initialize an electrum client.
		const electrum = new ElectrumClient('Electrum transport test', minVersion, host, port, ElectrumTransport[scheme].Scheme);

		//
		const failOnError = function(error)
		{
			// Fails due to connection error.
			t.fail(error.message);
		};

		electrum.on('error', failOnError);

		// Wait for the client to connect
		await electrum.connect();

		// Close the connection synchronously.
		await electrum.disconnect();

		// Pass the test since the we successfully made a connection with the given transport.
		t.pass();
	}
	catch(error)
	{
		// Fail the test due to encountered error.
		t.fail(error);
	}
};

// Set up transport tests.
const runTransportTests = async function()
{
	// For each server to test..
	for(const server of serverList)
	{
		// .. and for each transport the server supports..
		for(const transport in server.transports)
		{
			// .. perform a connection test with the server.
			test.serial(`Establish connection to ${server.host} with ${transport} transport on port ${server.transports[transport]}`, testServerTransport.bind(null, server.host, server.transports[transport], transport));
		}
	}
};

// Run the transport tests.
runTransportTests();
