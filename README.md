# What is this?

This repository houses a list of electrum servers operating on the Bitcoin Cash (BCH) network. The list can be used by anyone, but is intended to provide a default set of servers for the `electrum-cash` library.

# How can I contribute?

If you want to help maintain this list you can do so by submitting issues or pull requests.

## Remove stale servers

To remove a server that is no longer available or is no longer running on the Bitcoin Cash (BCH) network, submit an issue according explaining why the server should no longer be used.

## Add new servers

To add a new server to the list, make sure it is publically accessible and operates on the Bitcoin Cash (BCH) network, then submit an issue and explain why it should be added and what port numbers it uses for the connection methods it supports.
